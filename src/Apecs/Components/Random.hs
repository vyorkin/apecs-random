{-# LANGUAGE TypeFamilies #-}

module Apecs.Components.Random
  ( RandomGen(..)
  ) where

import Apecs
import System.IO.Unsafe
import System.Random.MWC.Probability

newtype RandomGen = RandomGen GenIO

instance Component RandomGen where
  type Storage RandomGen = Global RandomGen

instance Semigroup RandomGen where
  _a <> b = b

instance Monoid RandomGen where
  mempty = RandomGen defaultGenIO

defaultGenIO :: GenIO
defaultGenIO = unsafePerformIO create
{-# NOINLINE defaultGenIO #-}
