{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}

module Apecs.System.Random
  ( -- * Generator management
    initialize
  , save
  , restore

    -- * Lifted sampling
  , sample
  , samples

    -- * Common samples

    -- ** Uniform
  , range
  , bounded
  , boundedEnum
  , pick
  ) where

import Apecs
import Apecs.Components.Random

import qualified Data.List.NonEmpty as NonEmpty
import qualified System.Random.MWC.Probability as MWC

initialize :: (Get w IO RandomGen) => SystemT w IO ()
initialize = do
  genIO <- liftIO MWC.createSystemRandom
  set global $ RandomGen genIO

save :: (Get w IO RandomGen) => SystemT w IO MWC.Seed
save = do
  RandomGen genIO <- get global
  liftIO $ MWC.save genIO

restore :: (Set w IO RandomGen) => MWC.Seed -> SystemT w IO ()
restore seed = do
  genIO <- liftIO $ MWC.restore seed
  set global $ RandomGen genIO

withRandom :: Get w IO RandomGen => (MWC.GenIO -> SystemT w IO a) -> SystemT w IO a
withRandom proc = do
  RandomGen genIO <- get global
  proc genIO

sample :: Get w IO RandomGen => MWC.Prob IO a -> SystemT w IO a
sample model = withRandom $
  liftIO . MWC.sample model
{-# INLINABLE sample #-}

samples :: Get w IO RandomGen => Int -> MWC.Prob IO a -> SystemT w IO [a]
samples n model = withRandom $
  liftIO . sequenceA . replicate n . MWC.sample model
{-# INLINABLE samples #-}

range :: (MWC.Variate a, Get w IO RandomGen) => (a, a) -> SystemT w IO a
range = sample . MWC.uniformR
{-# INLINE range #-}

bounded :: (Bounded a, MWC.Variate a, Get w IO RandomGen) => SystemT w IO a
bounded = range (minBound, maxBound)
{-# INLINE bounded #-}

boundedEnum :: forall a w . (Enum a, Bounded a, Get w IO RandomGen) => SystemT w IO a
boundedEnum = fmap toEnum $ range
  ( fromEnum @a minBound
  , fromEnum @a maxBound
  )
{-# INLINE boundedEnum #-}

pick :: Get w IO RandomGen => NonEmpty.NonEmpty a -> SystemT w IO a
pick xs = do
  i <- range (0, NonEmpty.length xs - 1)
  pure $ xs NonEmpty.!! i
{-# INLINE pick #-}
